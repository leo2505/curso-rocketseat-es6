class List {
    constructor() {
        this.data = []
    }
    add(data) {
        this.data.push(data)
        console.log(this.data)
    }
}

class TodoList extends List {
    constructor() {
        super()

        this.usuario = 'Leonardo'
    }

    mostraUsuario() {
        console.log(this.usuario)
    }

}





class Arrays {
    constructor() {
        this.arr = []
    }
    valor(arr) {
        const newArr = arr.map(item => {
            return `Adicionados: ${item}`
        })
        console.log(newArr)
    }
}


const MinhaLista = new TodoList()
const arrays = new Arrays()

document.getElementById('novotodo').onclick = () => {
    MinhaLista.add('2')
    let add = arrays.arr.push(MinhaLista.data.length)
    arrays.valor([add])
}
MinhaLista.mostraUsuario()


const usuario = {
    nome: 'Leonardo',
    serie: '19238710',
    numero: 7328372,
    endereco: {
        rua: 'xyz',
        numeroEnd: 3
    }
}

const { nome, serie, numero, endereco: { rua, numeroEnd } } = usuario
console.log(`Nome: ${nome} | Série: ${serie} | Número: ${numero} | Rua: ${rua} Nº: ${numeroEnd}`)


function mostraNome({ nome }) {
    console.log(`Nome no objeto: `, nome)
}
mostraNome(usuario)

const rester = {
    item: 1,
    nome: 'rest',
    versao: 'ecmascript 6'
}
const { item, ...resto } = rester
console.log(item)
console.log(resto)


// para não precisar add parametros toda vez:
function soma(...params) {
    return params.reduce((total, next) => total + next)
}
console.log(soma(1, 2, 3))
console.log(soma(1, 2, 3, 4, 5, 6))

//Object Short Syntax
const nome_s = 'Leo'
const idade_s = 27

const usuario_s = {
    nome_s,
    idade_s
}
console.log(usuario_s)


//PROMISE!

const minhaPromise = () => new Promise((resolve, reject) => {
    setTimeout(() => { resolve(' Promise Ok') }, 2000)
})

minhaPromise()
    .then(response => {
        console.log(response)
    })
    .catch(err => {
        console.log(err)
    })


//OU UTILIZAR ASYNC AWAIT... ES8
// const minhaPromise2 = () => new Promise((resolve, reject) => {
//     setTimeout(() => { resolve('Promise async await OK') }, 2000)
// })

// const executaPromise2 = async () => {
//     console.log(await minhaPromise2())
// }
// executaPromise2()